Note: We are using JavaSE-1.8 for the development of this project.

Code Overview :
	code is divided in following packages.
		com.ai.Classifier.KNN
			This package contains all classes and logics for KNN.
			
		com.ai.Classifier.NaiveBayes
			This package contains all classes and logics for NaiveBayes.
			
		com.ai.Classifier.NeuralNetwork
			This package contains all classes and logics for NeuralNetwork.
			
		com.ai.Classifier.SVM
			This package contains all classes and logics for SVM.
			
		com.ai.Clustering.KMean
			This package contains all classes and logics for KMean.
			
		com.ai.config
			This package contains the all configuration setting like file path etc..
			
		com.ai.FeatureExtraction
			This package contains all classes and logics for Feature Extraction.
			It extract features and convert into CSV file.
			
		com.ai.IOFiling
			contain logic for file save and read.
			
		com.ai.Main
			this package has multiple classes for run the respective classifier with name 
			of class.
			
		com.ai.Model
			Contains the model getter and setter class.

How to Run the Program :
	First go to package 'com.ai.Main' and select a classifier class that you want to execute.
	suppose we have select 'ProgramKNN.java' to run.
	Here just uncomment the features file path that you want to test and comment the rest file path. Each Program class has engouh comments you will understand how to execute.
	After selecting respective file path just run it.
	
	This package has following classes.
	ProgramCluster.java
		Execution and implementation of K-Mean Clustering.
		
	ProgramGenerateCSVFeatureVectore.java
		Execution Feature Extraction process.
		
	ProgramKNN.java
		Execution and implementation of KNN.
		
	ProgramMPLClassifiers.java
		Execution and implementation of Multilayer Perceptron.
		In Project directory '\aidigitclassifiers' you can find multiple '.zip' files. These files contains the 
		pre-trained data.If you want to trained again the network then remove all '.zip' from 
		Project directory.
		
	ProgramNaiveBayes.java
		Execution and implementation of Multilayer Naive Bayes.
		
	ProgramSVM.
		Execution and implementation of Multilayer SVM.
		
	TestCaseReadImagesAndGetMatrix.java
		Raw and rough testing class.
		
Data :
	All type of data that we need to train, test the algorithms are available in 
	Project 'aidigitclassifiers\data' directory.
	
Feature Extraction :
	To extract features, execute com.ai.Main.ProgramMPLClassifiers.java file but in order to 
	successful execution please take care of following guide lines.
	1) First goto com.ai.config.ApplicationStatic.java file and set or verify the values of 
	following variables.
	2) FOLDERS : set folder name you have like Testing, Training and Validation.
	3) GENERIC_PATH : set generic path to reach these folders.
	4) FOLDER_NUMBERS : set number that you have inside of Testing, Training and Validation.
	5) IMG_WIDTH : Set the image width.
	6) IMG_HEIGHT : set the images height.
	7) SUPPORTED_IMG_FRMT : what type of format your images have.
	
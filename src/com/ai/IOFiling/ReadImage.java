package com.ai.IOFiling;

import java.awt.AlphaComposite;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.awt.image.Raster;
import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.Random;

import javax.imageio.ImageIO;

import org.datavec.api.io.labels.ParentPathLabelGenerator;
import org.datavec.api.split.FileSplit;
import org.datavec.image.loader.NativeImageLoader;
import org.datavec.image.recordreader.ImageRecordReader;
import org.deeplearning4j.datasets.datavec.RecordReaderDataSetIterator;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.dataset.DataSet;
import org.nd4j.linalg.dataset.api.iterator.DataSetIterator;
import org.nd4j.linalg.dataset.api.preprocessor.DataNormalization;
import org.nd4j.linalg.dataset.api.preprocessor.ImagePreProcessingScaler;

public class ReadImage {

	public double[][] getNormalizeMatrix(File file, int imgWidth, int imgHeight) {
		try {

			BufferedImage originalImage = ImageIO.read(file);
			int type = originalImage.getType() == 0 ? BufferedImage.TYPE_INT_ARGB : originalImage.getType();
			BufferedImage resizeImageJpg = resizeImageWithHint(originalImage, type, imgWidth, imgHeight);
			Raster raster = resizeImageJpg.getData();
			int w = raster.getWidth(), h = raster.getHeight();
			double pixels[][] = new double[w][h];
			for (int x = 0; x < w; x++) {
				for (int y = 0; y < h; y++) {
					pixels[x][y] = raster.getSample(x, y, 0);
				}
			}
			return pixels;

		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	private BufferedImage resizeImageWithHint(BufferedImage originalImage, int type, int imgWidth,
			int imgHeight) {

		BufferedImage resizedImage = new BufferedImage(imgWidth, imgHeight, type);
		Graphics2D g = resizedImage.createGraphics();
		g.drawImage(originalImage, 0, 0, imgWidth, imgHeight, null);
		g.dispose();
		g.setComposite(AlphaComposite.Src);

		g.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
		g.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

		return resizedImage;
	}
	
	public double[][] getNormalizeMatrixD4j(File file, int width, int height) throws IOException {

		int channels = 1;
		int rngseed = 123;
		Random randNumGen = new Random(rngseed);
		int batchSize = 1;
		int outputNum = 10;

		FileSplit train = new FileSplit(file, NativeImageLoader.ALLOWED_FORMATS, randNumGen);
		ParentPathLabelGenerator labelMaker = new ParentPathLabelGenerator();
		ImageRecordReader recordReader = new ImageRecordReader(height, width, channels, labelMaker);
		recordReader.initialize(train);
		DataSetIterator dataIter = new RecordReaderDataSetIterator(recordReader, batchSize, 1, outputNum);

		// Scale pixel values to 0-1

		DataNormalization scaler = new ImagePreProcessingScaler(0, 1);
		scaler.fit(dataIter);
		dataIter.setPreProcessor(scaler);
		DataSet ds = dataIter.next();
		INDArray arr = ds.getFeatureMatrix();
		/*
		System.out.println(arr);
		System.out.println(arr.getDouble(762));
		System.out.println(ds.getLabels());
*/
		double n = 123.45678;
		DecimalFormat df = new DecimalFormat("#.##");

		double[][] matrix = new double[width][height];
		int index = 0;
		for (int h = 0; h < height; h++) {
			for (int w = 0; w < width; w++) {
				// System.out.println("Index: "+index+", w:"+w+", h:"+h);
				matrix[h][w] = Double.valueOf(df.format(arr.getDouble(index)));
				++index;
			}
		}
		return matrix;
	}
}

package com.ai.IOFiling;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.List;

import com.ai.Model.DigitImage;

public class SaveFile {
	
	public void saveInCSV(List<DigitImage> data, String filePath) throws FileNotFoundException {
		PrintWriter pw = new PrintWriter(new File(filePath));
		StringBuilder sb = new StringBuilder();
		int count = data.size();
		for (int i = 0; i < count; i++) {
			int lable = data.get(i).getLabel();
			String commaSeperatedData = convetToCommaSeperated(data.get(i).getData());
			sb.append(lable + "," + commaSeperatedData);
			sb.append('\n');
		}
		pw.write(sb.toString());
		pw.close();
	}

	private static String convetToCommaSeperated(double[] data) {
		String value = "";
		for (double d : data) {
			value += d + ",";
		}
		value = value.trim().substring(0, value.length() - 1);
		return value;
	}
}

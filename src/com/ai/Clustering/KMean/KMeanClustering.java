package com.ai.Clustering.KMean;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.PrintStream;

import weka.clusterers.ClusterEvaluation;
import weka.clusterers.SimpleKMeans;
import weka.core.Instances;

public class KMeanClustering {

	private BufferedReader readDataFile(String filename) {
		BufferedReader inputReader = null;

		try {
			inputReader = new BufferedReader(new FileReader(filename));
		} catch (FileNotFoundException ex) {
			System.err.println("File not found: " + filename);
		}

		return inputReader;
	}

	public void makeCluster(String trainingFilePath, String testingFilePath, int numberOfCluster, String logFilePath) throws Exception {
		SimpleKMeans kmeans = new SimpleKMeans();

		kmeans.setSeed(10);

		// important parameter to set: preserver order, number of cluster.
		kmeans.setPreserveInstancesOrder(true);
		// kmeans.setNumClusters(10);
		kmeans.setNumClusters(numberOfCluster);

		// BufferedReader datafile =
		// readDataFile("C:/Users/SajjadAshrafCan/Downloads/weka-3-7-12/weka-3-7-12/data/contact-lenses.arff");
		// BufferedReader datafile =
		// readDataFile("C:/Users/SajjadAshrafCan/Downloads/weka-3-7-12/weka-3-7-12/data/HandDigitData/TrainAllPixels.arff");
		// BufferedReader datafile =
		// readDataFile("C:/Users/SajjadAshrafCan/Downloads/weka-3-7-12/weka-3-7-12/data/AllPixels.csv");
		BufferedReader trainingData = readDataFile(trainingFilePath);
		Instances intanceTrainingdata = new Instances(trainingData);

		kmeans.buildClusterer(intanceTrainingdata);

		// This array returns the cluster number (starting with 0) for each
		// instance
		// The array has as many elements as the number of instances
		int[] assignments = kmeans.getAssignments();

		// try (PrintStream out = new PrintStream(new
		// FileOutputStream("Log.txt"))) {
		try (PrintStream out = new PrintStream(new FileOutputStream(logFilePath))) {
			int i = 0;
			for (int clusterNum : assignments) {
				// System.out.printf("Instance %d -> Cluster %d \n", i,
				// clusterNum);
				out.print(String.format("Instance %d -> Cluster %d \r\n", i, clusterNum));
				i++;
			}

			
			BufferedReader testingData = readDataFile(testingFilePath);
			Instances intanceTestingdata = new Instances(testingData);
			
			// evaluate clusterer
			ClusterEvaluation eval = new ClusterEvaluation();
			eval.setClusterer(kmeans);
			eval.evaluateClusterer(intanceTestingdata);
			// print results
			String clusterResult = eval.clusterResultsToString().replace("\n", "\r\n");
			System.out.println(eval.clusterResultsToString());
			out.print(clusterResult);
		}
	}
}
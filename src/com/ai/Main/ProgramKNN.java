package com.ai.Main;

import com.ai.Classifier.KNN.KNNClassifier;

/**
 * KNN Test Class
 *
 */
public class ProgramKNN {
	static String trainingFilePath = "";
	static String testingFilePath = "";

	public static void main(String[] args) throws Exception {
		/**
		 * To Test Specific Extraction Technique Kindly uncomment the respective
		 * file paths.
		 */

		/**
		 * KNNClassifier for AllPixels Image Size 28*28 Feature Extraction
		 * Strategy.
		 */
		// trainingFilePath = "data/Training/AllPixels.csv";
		// testingFilePath = "data/Testing/AllPixels.csv";

		/**
		 * KNNClassifier for AllPixels Image Size 16*16 Feature Extraction
		 * Strategy.
		 */
		//trainingFilePath = "data/Training/AllPixels_16_16.csv";
		//testingFilePath = "data/Testing/AllPixels_16_16.csv";

		/**
		 * KNNClassifier for PixelsZone4IMG16 Feature Extraction Strategy.
		 */
		 trainingFilePath = "data/Training/PixelsZone4IMG16.csv";
		 testingFilePath = "data/Testing/PixelsZone4IMG16.csv";

		/**
		 * KNNClassifier for PixelsZone4IMG32 Feature Extraction Strategy
		 */
		// trainingFilePath = "data/Training/PixelsZone4IMG32.csv";
		// testingFilePath = "data/Testing/PixelsZone4IMG32.csv";

		/**
		 * KNNClassifier for SUMOfPixels Feature Extraction Strategy
		 */
		// trainingFilePath = "data/Training/SUMOfPixels.csv";
		// testingFilePath = "data/Testing/SUMOfPixels.csv";

		System.out.print("Training File Path :"+trainingFilePath+"\r\n");
		System.out.print("Testing File Path :"+testingFilePath+"\r\n");
		KNNClassifier knnClassifier = new KNNClassifier();
		knnClassifier.Classify(trainingFilePath, testingFilePath);

	}
}
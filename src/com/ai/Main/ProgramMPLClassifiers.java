package com.ai.Main;

import com.ai.Classifier.NeuralNetwork.BaseClassifier;
import com.ai.Classifier.NeuralNetwork.ClassifierFectroy;
import com.ai.config.Enums.Classifiers;

public class ProgramMPLClassifiers {

	static String trainingFilePath = "";
	static String testingFilePath = "";

	/**
	 * Path (dirTrain, dirTest) of Directory Which contains actual Images but in PNG format. and
	 * line # 76 replace with.
	 * Classifiers classifiersAlgo =  Classifiers.MPLImagePipeline;
	 * This Classifiers directly read images and
	 * trained the neural network.
	 */
	static String dirTrain = "C:/AI/Persian_Handwritten_digits_Samples/Training";
	static String dirTest = "C:/AI/Persian_Handwritten_digits_Samples/Testing";
	static double rate = 0.0015;
	static double learningRate = 0.01;
	// static int height = 2;
	// static int width = 29;
	static int rngseed = 123;
	static int batchSize = 128;
	static int outputNum = 10;
	static int numEpochs = 100;
	static int rngSeed = 123;
	static int numRows = 2;
	static int numColumns = 28;

	public static void main(String[] args) throws Exception {

		/**
		 * To Test Specific Extraction Technique Kindly uncomment the respective
		 * file paths.
		 * If you want to train the network from start delete all .zip file from
		 * Project directory like  'AllPixels_16_16_SMPL.zip', 
		 * 'PixelsZone4IMG16_SMPL.zip' etc...
		 */

		/**
		 * MultipleLayerPerceptron for AllPixels Image Size 28*28 Feature
		 * Extraction Strategy.
		 */
		// trainingFilePath = "data/Training/AllPixels.csv";
		// testingFilePath = "data/Testing/AllPixels.csv";

		/**
		 * MultipleLayerPerceptron for AllPixels Image Size 16*16 Feature
		 * Extraction Strategy.
		 */
		trainingFilePath = "data/Training/AllPixels_16_16.csv";
		testingFilePath = "data/Testing/AllPixels_16_16.csv";

		/**
		 * MultipleLayerPerceptron for PixelsZone4IMG16 Feature Extraction
		 * Strategy.
		 */
		// trainingFilePath = "data/Training/PixelsZone4IMG16.csv";
		// testingFilePath = "data/Testing/PixelsZone4IMG16.csv";

		/**
		 * MultipleLayerPerceptron for PixelsZone4IMG32 Feature Extraction
		 * Strategy
		 */
		// trainingFilePath = "data/Training/PixelsZone4IMG32.csv";
		// testingFilePath = "data/Testing/PixelsZone4IMG32.csv";

		/**
		 * MultipleLayerPerceptron for SUMOfPixels Feature Extraction Strategy
		 */
		// trainingFilePath = "data/Training/SUMOfPixels.csv";
		// testingFilePath = "data/Testing/SUMOfPixels.csv";

		/**
		 * MultipleLayerPerceptron for Two layers
		 */
		// Classifiers classifiersAlgo = Classifiers.MPLTwoLayer;

		Classifiers classifiersAlgo = Classifiers.MPLSingleLayer;
		BaseClassifier baseClassifier = new ClassifierFectroy().getClassifier(classifiersAlgo);
		System.out.print("Training File Path :"+trainingFilePath+"\r\n");
		System.out.print("Testing File Path :"+testingFilePath+"\r\n");
		baseClassifier.Classify(trainingFilePath, testingFilePath, outputNum, batchSize, numEpochs, rngSeed, numRows,
				numColumns);

	}
}

package com.ai.Main;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.ai.FeatureExtraction.ExtractFectory;
import com.ai.FeatureExtraction.IFeatureExtraction;
import com.ai.IOFiling.ReadImage;
import com.ai.IOFiling.SaveFile;
import com.ai.Model.DigitImage;
import com.ai.config.ApplicationStatic;
import com.ai.config.Enums.FeatureExteraction;

public class ProgramGenerateCSVFeatureVectore {

	public static void main(String[] args) throws IOException {
		/**
		 * First goto com.ai.config.ApplicationStatic.java file and set or
		 * verify the values of following variables.
		 * 
		 * FOLDERS : set folder name you have like Testing, Training and
		 * Validation.
		 * 
		 * GENERIC_PATH : set generic path to reach these folders.
		 * 
		 * FOLDER_NUMBERS : set number that you have inside of Testing, Training
		 * and Validation.
		 * 
		 * IMG_WIDTH : Set the image width.
		 * 
		 * IMG_HEIGHT : set the images height.
		 * 
		 * SUPPORTED_IMG_FRMT : what type of format your images have.
		 */

		/**
		 * To Generate All Type Features Extraction in once, uncomment following
		 * code.
		 */
		// GenerateAllFeactureExtration();

		/**
		 * To Generate Specific Features Extraction pass a respective
		 * FeatureExteraction type, execute following code.
		 */
		GenerateForOneFeactureExtration(FeatureExteraction.AllPixels);

		/**
		 * other examples.
		 */
		// GenerateForOneFeactureExtration(FeatureExteraction.PixelsZone4IMG16);
		// GenerateForOneFeactureExtration(FeatureExteraction.PixelsZone4IMG32);
		// GenerateForOneFeactureExtration(FeatureExteraction.SUMOfPixels);

	}

	private static void GenerateAllFeactureExtration() throws IOException {
		for (FeatureExteraction extertaction : FeatureExteraction.values()) {
			GenerateForOneFeactureExtration(extertaction);
		}
	}

	private static void GenerateForOneFeactureExtration(FeatureExteraction extertaction) throws IOException {
		String dirPath = "";
		ReadImage readImg = new ReadImage();
		SaveFile saveFile = new SaveFile();

		IFeatureExtraction featureExtraction = new ExtractFectory().getFeatureExtraction(extertaction);
		int folderCout = ApplicationStatic.FOLDERS.length;

		for (int f = 0; f < folderCout; f++) {
			String folder = ApplicationStatic.FOLDERS[f];
			dirPath = String.format(ApplicationStatic.GENERIC_PATH + "%s/", folder);
			List<DigitImage> images = new ArrayList<DigitImage>();

			int numberCount = ApplicationStatic.FOLDER_NUMBERS.length;
			for (int i = 0; i < numberCount; i++) {
				int currentNum = ApplicationStatic.FOLDER_NUMBERS[i];
				String path = String.format(dirPath + "%d", currentNum);
				File[] files = new File(path).listFiles();

				double[][] matrix;

				for (File file : files) {
					if (file.isFile() && file.getName().toLowerCase().endsWith(ApplicationStatic.SUPPORTED_IMG_FRMT)) {

						if (extertaction.equals(FeatureExteraction.PixelsZone4IMG32)) {
							matrix = readImg.getNormalizeMatrix(file, 32, 32);
						} else {
							matrix = readImg.getNormalizeMatrix(file, ApplicationStatic.IMG_WIDTH,
									ApplicationStatic.IMG_HEIGHT);
						}

						images.add(featureExtraction.Extract(matrix, currentNum));

						System.out.println("FileName : " + file.getName());
					}
				}
				System.out.println("****** Folder " + i + " Path : " + path + " *********");
			}

			saveFile.saveInCSV(images, dirPath + extertaction.toString() + ".csv");
			System.out.println("images Size (folders) : " + images.size());
		}

	}

}

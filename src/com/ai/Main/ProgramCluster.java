package com.ai.Main;

import com.ai.Clustering.KMean.KMeanClustering;

public class ProgramCluster {
	static String trainingFilePath = "";
	static String testingFilePath = "";
	static String logFilePath = "KMeanClusteringLog.txt";
	static int numberOfCluster = 10;

	public static void main(String[] args) throws Exception {

		/**
		 * To Test Specific Extraction Technique Kindly uncomment the respective
		 * file paths. To Convert CSV File into '.arff' format visit
		 * http://ikuz.eu/csv2arff/.
		 */

		/**
		 * K-Mean for AllPixels Feature Extraction Strategy.
		 */
		trainingFilePath = "data/Training/AllPixels.arff";
		testingFilePath = "data/Testing/AllPixels.arff";

		/**
		 * K-Mean for PixelsZone4IMG16 Feature Extraction Strategy.
		 */
		// trainingFilePath = "data/Training/PixelsZone4IMG16.arff";
		// testingFilePath = "data/Testing/PixelsZone4IMG16.arff";

		/**
		 * K-Mean for PixelsZone4IMG32 Feature Extraction Strategy
		 */
		// trainingFilePath = "data/Training/PixelsZone4IMG32.arff";
		// testingFilePath = "data/Testing/PixelsZone4IMG32.arff";

		/**
		 * K-Mean for SUMOfPixels Feature Extraction Strategy
		 */
		// trainingFilePath = "data/Training/SUMOfPixels.arff";
		// testingFilePath = "data/Testing/SUMOfPixels.arff";

		/**
		 * out put of clustering will save in logFilePath.
		 */
		System.out.print("Training File Path :"+trainingFilePath+"\r\n");
		System.out.print("Testing File Path :"+testingFilePath+"\r\n");
		KMeanClustering kMean = new KMeanClustering();
		kMean.makeCluster(trainingFilePath, testingFilePath, numberOfCluster, logFilePath);
	}
}

package com.ai.Main;

import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.Random;

import org.datavec.api.io.labels.ParentPathLabelGenerator;
import org.datavec.api.split.FileSplit;
import org.datavec.image.loader.NativeImageLoader;
import org.datavec.image.recordreader.ImageRecordReader;
import org.deeplearning4j.datasets.datavec.RecordReaderDataSetIterator;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.dataset.DataSet;
import org.nd4j.linalg.dataset.api.iterator.DataSetIterator;
import org.nd4j.linalg.dataset.api.preprocessor.DataNormalization;
import org.nd4j.linalg.dataset.api.preprocessor.ImagePreProcessingScaler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This code example is featured in this youtube video
 * https://www.youtube.com/watch?v=ECA6y6ahH5E
 *
 ** This differs slightly from the Video Example, The Video example had the data
 * already downloaded This example includes code that downloads the data
 *
 *
 * The Data Directory mnist_png will have two child directories training and
 * testing The training and testing directories will have directories 0-9 with
 * 28 * 28 PNG images of handwritten images
 *
 *
 *
 * The data is downloaded from wget
 * http://github.com/myleott/mnist_png/raw/master/mnist_png.tar.gz followed by
 * tar xzvf mnist_png.tar.gz
 *
 *
 *
 * This examples builds on the MnistImagePipelineExample by adding a Neural Net
 */
public class TestCaseReadImagesAndGetMatrix {
	private static Logger log = LoggerFactory.getLogger(TestCaseReadImagesAndGetMatrix.class);

	public static void main(String[] args) throws Exception {
		// File file = new
		// File("C:/AI/Persian_Handwritten_digits_Samples/Training/0/ID00001_p1_B31_cc1.png");
		File file = new File("C:/AI/Persian_Handwritten_digits_Samples/Training/1/ID00001_p2_B13_cc2.png");
		// double[][] matrix = getMartix(28, 28, file);
		double[][] matrix = getMartix(28, 28, file);
		System.out.println(Arrays.deepToString(matrix));
	}

	public static double[][] getMartix(int height, int width, File file) throws IOException {

		int channels = 1;
		int rngseed = 123;
		Random randNumGen = new Random(rngseed);
		int batchSize = 1;
		int outputNum = 10;

		FileSplit train = new FileSplit(file, NativeImageLoader.ALLOWED_FORMATS, randNumGen);
		ParentPathLabelGenerator labelMaker = new ParentPathLabelGenerator();
		ImageRecordReader recordReader = new ImageRecordReader(height, width, channels, labelMaker);
		recordReader.initialize(train);
		DataSetIterator dataIter = new RecordReaderDataSetIterator(recordReader, batchSize, 1, outputNum);

		// Scale pixel values to 0-1

		DataNormalization scaler = new ImagePreProcessingScaler(0, 1);
		scaler.fit(dataIter);
		dataIter.setPreProcessor(scaler);
		DataSet ds = dataIter.next();
		INDArray arr = ds.getFeatureMatrix();
		System.out.println(arr);
		System.out.println(arr.getDouble(762));
		System.out.println(ds.getLabels());

		double n = 123.45678;
		DecimalFormat df = new DecimalFormat("#.##");
		String dx = df.format(n);
		n = Double.valueOf(df.format(n));

		double[][] matrix = new double[width][height];
		int index = 0;
		for (int h = 0; h < height; h++) {
			for (int w = 0; w < width; w++) {
				// System.out.println("Index: "+index+", w:"+w+", h:"+h);
				matrix[h][w] = Double.valueOf(df.format(arr.getDouble(index)));
				++index;
			}
		}
		return matrix;
	}

}

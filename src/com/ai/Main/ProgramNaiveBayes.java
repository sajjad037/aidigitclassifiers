package com.ai.Main;

import java.util.List;

import com.ai.Classifier.NaiveBayes.NaiveBayesClassifier;
import com.ai.Classifier.NaiveBayes.Utility;

public class ProgramNaiveBayes {

	public static String trainingFilePath = "data/NaiveBayes/training/AllPixels.csv";
	public static String testingFilePath = "data/NaiveBayes/testing/AllPixels.csv";
	public static String OUTPUT_FILE = "result.txt";
	public static String stoplist;
	public static String trainingData;
	public static String trainingLabel;
	public static String testingData;
	public static String testingLabel;

	public static void main(String[] args) throws Exception {

		/**
		 * To Test Specific Extraction Technique Kindly uncomment the respective
		 * file paths. 
		 */
		
		/**
		 * NaiveBayes for AllPixels when Image Size 28*28 Feature Extraction
		 * Strategy.
		 */
		trainingFilePath = "data/NaiveBayes/training/AllPixels.csv";
		testingFilePath = "data/NaiveBayes/testing/AllPixels.csv";

		/**
		 * NaiveBayes for AllPixels when Image Size 16*16 Feature Extraction
		 * Strategy.
		 */
		// trainingFilePath = "data/NaiveBayes/training/AllPixels_16_16.csv";
		// testingFilePath = "data/NaiveBayes/testing/AllPixels_16_16.csv";

		System.out.print("Training File Path :"+trainingFilePath+"\r\n");
		System.out.print("Testing File Path :"+testingFilePath+"\r\n");
		List<int[]> trainingFeatures = Utility.trainfeature(trainingFilePath);
		NaiveBayesClassifier classifier = new NaiveBayesClassifier(trainingFeatures);

		List<int[]> testingFeaturesResult = classifier.classifyFeature(testingFilePath);
		double accuracyTestingLabel = classifier.calculateAccuracy(testingFeaturesResult, "testlabels.txt");

		System.out.println("Accuracy with testing data set is " + accuracyTestingLabel + "%.");

	}

}

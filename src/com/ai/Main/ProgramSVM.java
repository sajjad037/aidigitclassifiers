package com.ai.Main;

import com.ai.Classifier.SVM.SVMClassifier;;

public class ProgramSVM {

	static String trainingFilePath = "";
	static String testingFilePath = "";

	public static void main(String[] args) throws Exception {

		/**
		 * To Test Specific Extraction Technique Kindly uncomment the respective
		 * file paths.
		 */

		/**
		 * SVMClassifier for AllPixels Image Size 28*28 Feature Extraction
		 * Strategy.
		 */
		// trainingFilePath = "data/Training/AllPixels.csv";
		// testingFilePath = "data/Testing/AllPixels.csv";

		/**
		 * SVMClassifier for AllPixels Image Size 16*16 Feature Extraction
		 * Strategy.
		 */
		trainingFilePath = "data/Training/AllPixels_16_16.csv";
		testingFilePath = "data/Testing/AllPixels_16_16.csv";

		/**
		 * SVMClassifier for PixelsZone4IMG16 Feature Extraction Strategy.
		 */
		// trainingFilePath = "data/Training/PixelsZone4IMG16.csv";
		// testingFilePath = "data/Testing/PixelsZone4IMG16.csv";

		/**
		 * SVMClassifier for PixelsZone4IMG32 Feature Extraction Strategy
		 */
		// trainingFilePath = "data/Training/PixelsZone4IMG32.csv";
		// testingFilePath = "data/Testing/PixelsZone4IMG32.csv";

		/**
		 * SVMClassifier for SUMOfPixels Feature Extraction Strategy
		 */
		// trainingFilePath = "data/Training/SUMOfPixels.csv";
		// testingFilePath = "data/Testing/SUMOfPixels.csv";

		System.out.print("Training File Path :" + trainingFilePath + "\r\n");
		System.out.print("Testing File Path :" + testingFilePath + "\r\n");
		SVMClassifier svmClassifier = new SVMClassifier();
		svmClassifier.Classify(trainingFilePath, testingFilePath);
	}
}

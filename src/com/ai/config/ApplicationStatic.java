package com.ai.config;

public class ApplicationStatic {
	public static final String[] FOLDERS = { "Training", "Validation", "Testing" };
	public static final String GENERIC_PATH = "C:/Users/SajjadAshrafCan/Google Drive/Study/Concordia/Winter 2017/COMP-6721 INTRO TO A.I/Project/PublicProject/Persian_Handwritten_digits_Samples/";
	public static final int[] FOLDER_NUMBERS = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
	public static final int IMG_WIDTH = 28, IMG_HEIGHT = 28;
	public static final String SUPPORTED_IMG_FRMT = ".tif";
}

package com.ai.config;

public class Enums {

	public enum FeatureExteraction {
		AllPixelsImgSiz16_16, AllPixels, SUMOfPixels, PixelsZone4IMG16, PixelsZone4IMG32;
	}

	public enum Classifiers {
		MPLSingleLayer, MPLTwoLayer, MLPLinear, MPLImagePipeline;
	}
}

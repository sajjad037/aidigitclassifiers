package com.ai.Classifier.SVM;

import java.io.BufferedReader;
import java.io.FileReader;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import com.ai.Model.DigitImage;

import libsvm.svm;
import libsvm.svm_model;
import libsvm.svm_node;
import libsvm.svm_parameter;
import libsvm.svm_problem;

public class SVMClassifier {

	public void Classify(String trainingFilPath, String testingFilPath) throws Exception {
		// int[] numbers = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };

		// String filenameTrain = "C:/Users/SajjadAshrafCan/Google
		// Drive/Study/Concordia/Winter 2017/COMP-6721 INTRO TO
		// A.I/Project/PublicProject/Persian_Handwritten_digits_Samples/Training/SUMOfPixels.csv";
		// String filenameTest = "C:/Users/SajjadAshrafCan/Google
		// Drive/Study/Concordia/Winter 2017/COMP-6721 INTRO TO
		// A.I/Project/PublicProject/Persian_Handwritten_digits_Samples/Testing/SUMOfPixels.csv";

		List<DigitImage> dataTrain = readFile(trainingFilPath);
		List<DigitImage> datatest = readFile(testingFilPath);

		svm_model m = svmTrain(dataTrain);

		double[] ypred = svmPredict(datatest, m);

		double correct = 0;
		HashMap<String, Integer> hm = new HashMap<String, Integer>();
		String key = "";

		String[] languages = { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9" };
		List<String> languageList = new ArrayList<String>(Arrays.asList(languages));
		int[][] confusionMatrix = new int[10][10];

		for (int i = 0; i < datatest.size(); i++) {
			System.out.println("(Actual:" + datatest.get(i).getLabel() + " Prediction:" + ypred[i] + ")");
			confusionMatrix[datatest.get(i).getLabel()][(int) ypred[i]] += 1;
			if (datatest.get(i).getLabel() == ypred[i]) {
				correct++;
			}
			key = String.format("%d recognize as %f", datatest.get(i).getLabel(), ypred[i]);

			if (hm.containsKey(key)) {
				hm.put(key, hm.get(key) + 1);
			} else {
				hm.put(key, 1);
			}

		}

		correct = correct / (double) datatest.size();
		DecimalFormat df2 = new DecimalFormat("###.##");

		Map<String, Integer> treeMap = new TreeMap<String, Integer>(hm);
		for (Entry<String, Integer> m1 : treeMap.entrySet()) {
			System.out.println(m1.getKey() + ": " + m1.getValue());
		}

		StringBuilder stringBuilder = new StringBuilder();

		stringBuilder.append("-----------------------------------Confusion Matrix---------------------------------\n");
		stringBuilder.append(String.format("%5s", "") + "\t");
		for (String string : languageList) {
			stringBuilder.append(String.format("%5s", string) + "\t");
		}

		stringBuilder.append("\n");
		for (int i = 0; i < confusionMatrix.length; i++) {
			stringBuilder.append(String.format("%5s", languages[i]) + "\t");

			for (int j = 0; j < confusionMatrix[i].length; j++) {
				stringBuilder.append(String.format("%5s", confusionMatrix[i][j]) + "\t");

			}
			stringBuilder.append("\n");

		}

		System.out.println(stringBuilder);
		System.out.println("Accuracy: " + df2.format(correct * 100) + "%");
	}

	private svm_model svmTrain(List<DigitImage> dataTrain) {
		svm_problem prob = new svm_problem();
		int recordCount = dataTrain.size();
		int featureCount = dataTrain.get(0).getData().length;
		prob.y = new double[recordCount];
		prob.l = recordCount;
		prob.x = new svm_node[recordCount][featureCount];

		for (int i = 0; i < recordCount; i++) {
			double[] features = dataTrain.get(i).getData();

			prob.x[i] = new svm_node[features.length];
			for (int j = 0; j < features.length; j++) {
				svm_node node = new svm_node();
				node.index = j;
				node.value = features[j];
				prob.x[i][j] = node;
			}
			prob.y[i] = dataTrain.get(i).getLabel();
		}

		svm_parameter param = new svm_parameter();
		param.probability = 1;
		param.gamma = 0.5;
		param.nu = 0.5;
		param.C = 100;
		param.svm_type = svm_parameter.C_SVC;
		param.kernel_type = svm_parameter.LINEAR;
		param.cache_size = 20000;
		param.eps = 0.001;

		svm_model model = svm.svm_train(prob, param);

		return model;
	}

	private double[] svmPredict(List<DigitImage> dataTest, svm_model model) {

		double[] yPred = new double[dataTest.size()];

		for (int k = 0; k < dataTest.size(); k++) {

			double[] fVector = dataTest.get(k).getData();

			svm_node[] nodes = new svm_node[fVector.length];
			for (int i = 0; i < fVector.length; i++) {
				svm_node node = new svm_node();
				node.index = i;
				node.value = fVector[i];
				nodes[i] = node;
			}

			int totalClasses = 10;
			int[] labels = new int[totalClasses];
			svm.svm_get_labels(model, labels);

			double[] prob_estimates = new double[totalClasses];
			yPred[k] = svm.svm_predict_probability(model, nodes, prob_estimates);

		}

		return yPred;
	}

	private List<DigitImage> readFile(String labelFileName) throws Exception {
		List<DigitImage> images = new ArrayList<DigitImage>();
		BufferedReader br = null;
		String line = "";
		String cvsSplitBy = ",";

		br = new BufferedReader(new FileReader(labelFileName));
		while ((line = br.readLine()) != null) {

			// use comma as separator
			String[] values = line.split(cvsSplitBy);
			double[] nums = new double[values.length - 1];

			int lable = Integer.parseInt(values[0]);
			for (int i = 0; i < nums.length; i++) {
				nums[i] = Double.parseDouble(values[i + 1]);
			}

			images.add(new DigitImage(lable, nums));
		}

		return images;
	}

	private double[][] getData(String labelFileName) throws Exception {
		List<DigitImage> datatest = readFile(labelFileName);
		double[][] retData = new double[datatest.size()][datatest.get(0).getData().length];

		for (int i = 0; i < datatest.size(); i++) {
			for (int j = 0; j < datatest.get(i).getData().length; j++) {
				retData[i][j] = datatest.get(i).getData()[j];
			}
		}

		return retData;
	}

	private double[][] getLables(String labelFileName) throws Exception {
		List<DigitImage> datatest = readFile(labelFileName);
		double[][] retData = new double[datatest.size()][1];

		for (int i = 0; i < datatest.size(); i++) {
			retData[i][0] = datatest.get(i).getLabel();
		}

		return retData;
	}
}

package com.ai.Classifier.KNN;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.datavec.api.records.reader.RecordReader;
import org.datavec.api.records.reader.impl.csv.CSVRecordReader;
import org.datavec.api.split.FileSplit;
import org.deeplearning4j.datasets.datavec.RecordReaderDataSetIterator;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.dataset.DataSet;
//import org.deeplearning4j.datasets.iterator.DataSetIterator;
import org.nd4j.linalg.dataset.api.iterator.DataSetIterator;
import org.nd4j.linalg.factory.Nd4j;

/**
 * KNN Test Class
 *
 */
public class KNNClassifier {
	// private Logger log = LoggerFactory.getLogger(KNNClassifier.class);

	public void Classify(String filenameTrain, String filenameTest) throws IOException, InterruptedException {

		RecordReader rrTrain = new CSVRecordReader();
		rrTrain.initialize(new FileSplit(new File(filenameTrain)));
		DataSetIterator itertrain = new RecordReaderDataSetIterator(rrTrain, 6000, 0, 10);
		DataSet datasetTrain = itertrain.next();

		RecordReader rrTest = new CSVRecordReader();
		rrTest.initialize(new FileSplit(new File(filenameTest)));
		DataSetIterator iterTest = new RecordReaderDataSetIterator(rrTest, 2000, 0, 10);
		DataSet datasetTest = iterTest.next();

		INDArray features = datasetTest.getFeatureMatrix();
		KNN knn = new KNN(10);
		knn.fit(datasetTrain);
		// System.out.println("P,C");
		int[] prediction = knn.predict(features);

		String[] languages = { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9" };
		List<String> languageList = new ArrayList<String>(Arrays.asList(languages));
		int[][] confusionMatrix = new int[10][10];
		int numCorrect = 0;
		double percentCorrect = 0;
		for (int i = 0; i < features.rows(); i++) {
			// System.out.println(prediction[i] + "," +
			// Nd4j.getBlasWrapper().iamax(datasetTest.getLabels().getRow(i)));

			confusionMatrix[Nd4j.getBlasWrapper().iamax(datasetTest.getLabels().getRow(i))][prediction[i]] += 1;

			if (Nd4j.getBlasWrapper().iamax(datasetTest.getLabels().getRow(i)) == prediction[i]) {
				numCorrect++;
			}
		}

		percentCorrect = (numCorrect / (double) features.rows()) * 100;
		StringBuilder stringBuilder = new StringBuilder();

		stringBuilder.append("-----------------------------------Confusion Matrix---------------------------------\n");
		stringBuilder.append(String.format("%5s", "") + "\t");
		for (String string : languageList) {
			stringBuilder.append(String.format("%5s", string) + "\t");
		}

		stringBuilder.append("\n");
		for (int i = 0; i < confusionMatrix.length; i++) {
			stringBuilder.append(String.format("%5s", languages[i]) + "\t");

			for (int j = 0; j < confusionMatrix[i].length; j++) {
				stringBuilder.append(String.format("%5s", confusionMatrix[i][j]) + "\t");

			}
			stringBuilder.append("\n");

		}
		System.out.println(stringBuilder);
		System.out.println("Accuracy : " + percentCorrect);
	}

}
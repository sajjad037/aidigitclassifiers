package com.ai.Classifier.NaiveBayes;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class NaiveBayesClassifier {

	private boolean trained = false;
	private double[][] dataLearned;
	int count;
	int classIndex;

	public boolean isTrained() {
		return trained;
	}

	public NaiveBayesClassifier(List<int[]> trainingFeatures) {

		count = trainingFeatures.get(0).length - 1;
		classIndex = count;
		dataLearned = new double[10][count + 1];

		dataLearned[0][classIndex] = 1 / (double) 10;
		dataLearned[1][classIndex] = 1 / (double) 10;
		dataLearned[2][classIndex] = 1 / (double) 10;
		dataLearned[3][classIndex] = 1 / (double) 10;
		dataLearned[4][classIndex] = 1 / (double) 10;
		dataLearned[5][classIndex] = 1 / (double) 10;
		dataLearned[6][classIndex] = 1 / (double) 10;
		dataLearned[7][classIndex] = 1 / (double) 10;
		dataLearned[8][classIndex] = 1 / (double) 10;
		dataLearned[9][classIndex] = 1 / (double) 10;

		for (int i = 0; i < count; i++) {

			int oneCount = 0;
			int zeroCount = 0;
			int twoCount = 0;
			int threeCount = 0;
			int fourCount = 0;
			int fiveCount = 0;
			int sixCount = 0;
			int sevenCount = 0;
			int eightCount = 0;
			int nineCount = 0;

			for (int[] j : trainingFeatures) {
				if (j[i] == 1 && j[classIndex] == 1) {
					oneCount++;
				} else if (j[i] == 1 && j[classIndex] == 0) {
					zeroCount++;
				} else if (j[i] == 1 && j[classIndex] == 2) {
					twoCount++;
				} else if (j[i] == 1 && j[classIndex] == 3) {
					threeCount++;
				} else if (j[i] == 1 && j[classIndex] == 4) {
					fourCount++;
				} else if (j[i] == 1 && j[classIndex] == 5) {
					fiveCount++;
				} else if (j[i] == 1 && j[classIndex] == 6) {
					sixCount++;
				} else if (j[i] == 1 && j[classIndex] == 7) {
					sevenCount++;
				} else if (j[i] == 1 && j[classIndex] == 8) {
					eightCount++;
				} else if (j[i] == 1 && j[classIndex] == 9) {
					nineCount++;
				}

			}

			dataLearned[0][i] = (zeroCount + 1) / (double) (600);
			dataLearned[1][i] = (oneCount + 1) / (double) 600;
			dataLearned[2][i] = (twoCount + 1) / (double) (600);
			dataLearned[3][i] = (threeCount + 1) / (double) 600;
			dataLearned[4][i] = (fourCount + 1) / (double) (600);
			dataLearned[5][i] = (fiveCount + 1) / (double) 600;
			dataLearned[6][i] = (sixCount + 1) / (double) (600);
			dataLearned[7][i] = (sevenCount + 1) / (double) 600;
			dataLearned[8][i] = (eightCount + 1) / (double) (600);
			dataLearned[9][i] = (nineCount + 1) / (double) 600;
		}

		/*
		 * for(double[] d : dataLearned){ printRow(d); }
		 */
		trained = true;
	}

	public List<int[]> classifyFeature(String testingFilPath) throws Exception {
		if (trained == false) {
			throw new Exception("The classifier must be trained before attempting to classify a feature set.");
		}

		// GenerateFeature generator = new GenerateFeature(vocabulary);
		// List<int[]> testFeatures =
		// generator.GenerateAllFeatures(testDataFileName);

		List<int[]> testFeatures = Utility.testfeature(testingFilPath);

		for (int[] feature : testFeatures) {

			double[] digits = new double[10];

			for (int i = 0; i < digits.length; i++) {
				digits[i] = Math.log(1 / (double) 10);
			}

			for (int i = 0; i < count; i++) {
				for (int j = 0; j < digits.length; j++)
					digits[j] += Math.log((feature[i] == 1) ? dataLearned[j][i] : 1 - dataLearned[j][i]);

			}
			feature[classIndex] = Utility.findMaxArray(digits);
		}
		return testFeatures;
	}

	public double calculateAccuracy(List<int[]> features, String testLabelFileName) {
		double percentCorrect = 0;
		BufferedReader labelFile;
		String[] languages = { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9" };
		List<String> languageList = new ArrayList<String>(Arrays.asList(languages));
		int[][] confusionMatrix = new int[10][10];

		try {
			labelFile = new BufferedReader(new FileReader(testLabelFileName));
			String line;
			int index = 0;
			int numCorrect = 0;
			try {
				while ((line = labelFile.readLine()) != null && index < features.size()) {
					confusionMatrix[Integer.parseInt(line.trim())][features.get(index)[classIndex]] += 1;

					if (features.get(index)[classIndex] == Integer.parseInt(line.trim())) {
						numCorrect++;
					}
					index++;
				}
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			percentCorrect = (numCorrect / (double) features.size()) * 100;
			StringBuilder stringBuilder = new StringBuilder();

			stringBuilder
					.append("-----------------------------------Confusion Matrix---------------------------------\n");
			stringBuilder.append(String.format("%5s", "") + "\t");
			for (String string : languageList) {
				stringBuilder.append(String.format("%5s", string) + "\t");
			}
			stringBuilder.append("\n");

			for (int i = 0; i < confusionMatrix.length; i++) {
				stringBuilder.append(String.format("%5s", languages[i]) + "\t");

				for (int j = 0; j < confusionMatrix[i].length; j++) {
					stringBuilder.append(String.format("%5s", confusionMatrix[i][j]) + "\t");

				}
				stringBuilder.append("\n");

			}
			System.out.println(stringBuilder);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return percentCorrect;
	}

	public static void printRow(double[] row) {
		for (double i : row) {
			System.out.print(i);
			System.out.print("\t");
		}
		System.out.println();
	}
}

package com.ai.Classifier.NaiveBayes;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Utility {

	public static List<int[]> trainfeature(String csvFile) {

		// 28 * 28 Pixels
		//String csvFile = "training\\AllPixels.csv";

		// 16 * 16 Pixels
		// String csvFile = "training\\AllPixels_16_16.csv";

		String line = "";
		String cvsSplitBy = ",";

		List<int[]> list = new ArrayList<int[]>();

		try (BufferedReader br = new BufferedReader(new FileReader(csvFile))) {

			while ((line = br.readLine()) != null) {

				// use comma as separator
				String[] feature = line.split(cvsSplitBy);

				int[] intfeatureArray = new int[feature.length];

				for (int i = 0; i < feature.length; i++) {
					intfeatureArray[i] = Integer.parseInt(feature[i]);
				}

				list.add(intfeatureArray);

			}

			System.out.println("Training image count is :: " + list.size());
		} catch (IOException e) {
			e.printStackTrace();
		}

		return list;

	}

	public static List<int[]> testfeature(String csvFile) {

		// 28 * 28 Pixels
		//String csvFile = "testing\\testing.csv";

		// 16 * 16 Pixels
		// String csvFile = "testing\\AllPixels_16_16.csv";
		String line = "";
		String cvsSplitBy = ",";

		List<int[]> list = new ArrayList<int[]>();

		try (BufferedReader br = new BufferedReader(new FileReader(csvFile))) {

			while ((line = br.readLine()) != null) {

				// use comma as separator
				String[] feature = line.split(cvsSplitBy);

				int[] intfeatureArray = new int[feature.length];

				for (int i = 0; i < feature.length; i++) {
					intfeatureArray[i] = Integer.parseInt(feature[i]);
				}

				list.add(intfeatureArray);

			}

			System.out.println("Testing image count is :: " + list.size());
		} catch (IOException e) {
			e.printStackTrace();
		}

		return list;

	}

	public static int findMaxArray(double[] array) {

		int maxIndex = 0;
		for (int i = 1; i < array.length; i++) {
			double newnumber = array[i];
			if ((newnumber > array[maxIndex])) {
				maxIndex = i;
			}
		}

		return maxIndex;
	}

	public static void main(String[] args) {

		long value = 0l;

		if (value == 0l) {
			System.out.println("mandeep");
		}

		// System.out.println( TestCsv.findMaxArray(new
		// double[]{11.0,23.0,8.0}));

	}

}

package com.ai.Classifier.NeuralNetwork;

public interface BaseClassifier {

	void Classify(String filenameTrain, String filenameTest,  int outputNum, int batchSize, int numEpochs, int rngSeed, int numRows, int numColumns)throws Exception;
}

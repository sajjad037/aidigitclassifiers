package com.ai.Classifier.NeuralNetwork;

import java.io.File;

import org.datavec.api.records.reader.RecordReader;
import org.datavec.api.records.reader.impl.csv.CSVRecordReader;
import org.datavec.api.split.FileSplit;
import org.deeplearning4j.datasets.datavec.RecordReaderDataSetIterator;
import org.deeplearning4j.eval.Evaluation;
import org.deeplearning4j.nn.api.OptimizationAlgorithm;
import org.deeplearning4j.nn.conf.MultiLayerConfiguration;
import org.deeplearning4j.nn.conf.NeuralNetConfiguration;
import org.deeplearning4j.nn.conf.layers.DenseLayer;
import org.deeplearning4j.nn.conf.layers.OutputLayer;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.deeplearning4j.nn.weights.WeightInit;
import org.deeplearning4j.optimize.listeners.ScoreIterationListener;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.dataset.DataSet;
import org.nd4j.linalg.dataset.api.iterator.DataSetIterator;
import org.nd4j.linalg.lossfunctions.LossFunctions;

//////////////////////////////////////
// This is the version of MLPClassifierLinear for the screencast
// This example can also be found as part of a large collection 
// of examples at https://github.com/deeplearning4j/dl4j-examples.git
// with instructions on configuring your environment here
// http://deeplearning4j.org/quickstart
// More information at http://skymind.io/
//////////////////////////////////////

public class MLPLinearClassifier implements BaseClassifier {

	@Override
	public void Classify(String filenameTrain, String filenameTest, int outputNum, int batchSize, int numEpochs,
			int rngSeed, int numRows, int numColumns) throws Exception {
		System.out.println("Running MLPLinearClassifier.......");
		int seed = rngSeed;
		double learningRate = 0.01;
		int numInputs = numRows * numColumns;
		int numOutputs = outputNum;
		int numHiddenNodes = 2000;

		// load the training data
		RecordReader rr = new CSVRecordReader();
		rr.initialize(new FileSplit(new File(filenameTrain)));
		DataSetIterator trainIter = new RecordReaderDataSetIterator(rr, batchSize, 0, numOutputs);

		// load the test-evaluation data:

		RecordReader rrTest = new CSVRecordReader();
		rrTest.initialize(new FileSplit(new File(filenameTest)));
		DataSetIterator testIter = new RecordReaderDataSetIterator(rrTest, batchSize, 0, numOutputs);

		MultiLayerConfiguration conf = new NeuralNetConfiguration.Builder().seed(seed).iterations(1)
				.optimizationAlgo(OptimizationAlgorithm.STOCHASTIC_GRADIENT_DESCENT).learningRate(learningRate)
				.updater(org.deeplearning4j.nn.conf.Updater.NESTEROVS).momentum(0.9).list()
				.layer(0,
						new DenseLayer.Builder().nIn(numInputs).nOut(numHiddenNodes).weightInit(WeightInit.XAVIER)
								.activation("relu").build())
				.layer(1,
						new OutputLayer.Builder(LossFunctions.LossFunction.NEGATIVELOGLIKELIHOOD)
								.weightInit(WeightInit.XAVIER).activation("softmax").weightInit(WeightInit.XAVIER)
								.nIn(numHiddenNodes).nOut(numOutputs).build())
				.pretrain(false).backprop(true).build();

		MultiLayerNetwork model = new MultiLayerNetwork(conf);
		model.init();
		model.setListeners(new ScoreIterationListener(10));

		for (int n = 0; n < numEpochs; n++) {
			model.fit(trainIter);
		}

		System.out.println("Evaluate model.......");
		Evaluation eval = new Evaluation(numOutputs);
		while (testIter.hasNext()) {
			DataSet t = testIter.next();
			INDArray features = t.getFeatureMatrix();
			INDArray lables = t.getLabels();
			INDArray predicted = model.output(features, false);
			eval.eval(lables, predicted);
		}
		System.out.println(eval.stats());

	}

}

package com.ai.Classifier.NeuralNetwork;

import java.io.File;
import java.util.Random;

import org.datavec.api.io.labels.ParentPathLabelGenerator;
import org.datavec.api.split.FileSplit;
import org.datavec.image.loader.NativeImageLoader;
import org.datavec.image.recordreader.ImageRecordReader;
import org.deeplearning4j.datasets.datavec.RecordReaderDataSetIterator;
import org.deeplearning4j.eval.Evaluation;
import org.deeplearning4j.nn.api.OptimizationAlgorithm;
import org.deeplearning4j.nn.conf.MultiLayerConfiguration;
import org.deeplearning4j.nn.conf.NeuralNetConfiguration;
import org.deeplearning4j.nn.conf.Updater;
import org.deeplearning4j.nn.conf.inputs.InputType;
import org.deeplearning4j.nn.conf.layers.DenseLayer;
import org.deeplearning4j.nn.conf.layers.OutputLayer;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.deeplearning4j.nn.weights.WeightInit;
import org.deeplearning4j.optimize.listeners.ScoreIterationListener;
import org.nd4j.linalg.activations.Activation;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.dataset.DataSet;
import org.nd4j.linalg.dataset.api.iterator.DataSetIterator;
import org.nd4j.linalg.dataset.api.preprocessor.DataNormalization;
import org.nd4j.linalg.dataset.api.preprocessor.ImagePreProcessingScaler;
import org.nd4j.linalg.lossfunctions.LossFunctions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This code example is featured in this youtube video
 * https://www.youtube.com/watch?v=ECA6y6ahH5E
 *
 ** This differs slightly from the Video Example, The Video example had the data
 * already downloaded This example includes code that downloads the data
 *
 *
 * The Data Directory mnist_png will have two child directories training and
 * testing The training and testing directories will have directories 0-9 with
 * 28 * 28 PNG images of handwritten images
 *
 *
 *
 * The data is downloaded from wget
 * http://github.com/myleott/mnist_png/raw/master/mnist_png.tar.gz followed by
 * tar xzvf mnist_png.tar.gz
 *
 *
 *
 * This examples builds on the MnistImagePipelineExample by adding a Neural Net
 */
public class MnistImagePipelineExampleAddNeuralNet implements BaseClassifier {
	private static Logger log = LoggerFactory.getLogger(MnistImagePipelineExampleAddNeuralNet.class);

	@Override
	public void Classify(String dirTrain, String dirTest, int outputNum, int batchSize, int numEpochs, int rngSeed,
			int numRows, int numColumns) throws Exception {
		System.out.println("Running MnistImagePipelineExampleAddNeuralNet.......");
		// image information
		// 28 * 28 grayscale
		// grayscale implies single channel
		int height = numRows;
		int width = numColumns;
		int channels = 1;
		int rngseed = rngSeed;
		Random randNumGen = new Random(rngseed);

		File trainData = new File(dirTrain);
		File testData = new File(dirTest);

		// Define the FileSplit(PATH, ALLOWED FORMATS,random)

		FileSplit train = new FileSplit(trainData, NativeImageLoader.ALLOWED_FORMATS, randNumGen);
		FileSplit test = new FileSplit(testData, NativeImageLoader.ALLOWED_FORMATS, randNumGen);

		// Extract the parent path as the image label

		ParentPathLabelGenerator labelMaker = new ParentPathLabelGenerator();

		ImageRecordReader recordReader = new ImageRecordReader(height, width, channels, labelMaker);

		// Initialize the record reader
		// add a listener, to extract the name

		recordReader.initialize(train);
		// recordReader.setListeners(new LogRecordListener());

		// DataSet Iterator

		DataSetIterator dataIter = new RecordReaderDataSetIterator(recordReader, batchSize, 1, outputNum);

		// Scale pixel values to 0-1

		DataNormalization scaler = new ImagePreProcessingScaler(0, 1);
		scaler.fit(dataIter);
		dataIter.setPreProcessor(scaler);

		// Build Our Neural Network

		log.info("**** Build Model ****");

		MultiLayerConfiguration conf = new NeuralNetConfiguration.Builder().seed(rngseed)
				.optimizationAlgo(OptimizationAlgorithm.STOCHASTIC_GRADIENT_DESCENT).iterations(1).learningRate(0.006)
				.updater(Updater.NESTEROVS).momentum(0.9).regularization(true).l2(1e-4).list()
				.layer(0,
						new DenseLayer.Builder().nIn(height * width).nOut(100).activation(Activation.RELU)
								.weightInit(WeightInit.XAVIER).build())
				.layer(1,
						new OutputLayer.Builder(LossFunctions.LossFunction.NEGATIVELOGLIKELIHOOD).nIn(100)
								.nOut(outputNum).activation(Activation.SOFTMAX).weightInit(WeightInit.XAVIER).build())
				.pretrain(false).backprop(true).setInputType(InputType.convolutional(height, width, channels)).build();

		MultiLayerNetwork model = new MultiLayerNetwork(conf);

		// The Score iteration Listener will log
		// output to show how well the network is training
		model.setListeners(new ScoreIterationListener(10));

		log.info("*****TRAIN MODEL********");
		for (int i = 0; i < numEpochs; i++) {
			model.fit(dataIter);
		}

		log.info("******EVALUATE MODEL******");

		recordReader.reset();

		// The model trained on the training dataset split
		// now that it has trained we evaluate against the
		// test data of images the network has not seen

		recordReader.initialize(test);
		DataSetIterator testIter = new RecordReaderDataSetIterator(recordReader, batchSize, 1, outputNum);
		scaler.fit(testIter);
		testIter.setPreProcessor(scaler);

		/*
		 * log the order of the labels for later use In previous versions the
		 * label order was consistent, but random In current verions label order
		 * is lexicographic preserving the RecordReader Labels order is no
		 * longer needed left in for demonstration purposes
		 */
		log.info(recordReader.getLabels().toString());

		// Create Eval object with 10 possible classes
		Evaluation eval = new Evaluation(outputNum);

		// Evaluate the network
		while (testIter.hasNext()) {
			DataSet next = testIter.next();
			INDArray output = model.output(next.getFeatureMatrix());
			// Compare the Feature Matrix from the model
			// with the labels from the RecordReader
			eval.eval(next.getLabels(), output);

		}

		log.info(eval.stats());

	}
}

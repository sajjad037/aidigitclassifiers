package com.ai.Classifier.NeuralNetwork;

import com.ai.config.Enums.Classifiers;

public class ClassifierFectroy {
	public BaseClassifier getClassifier(Classifiers classifiers) {
		switch (classifiers) {
		case MLPLinear:
			return new MLPLinearClassifier();

		case MPLImagePipeline:
			return new MnistImagePipelineExampleAddNeuralNet();

		case MPLSingleLayer:
			return new MPLSingleLayerClassifier();

		case MPLTwoLayer:
			return new MPLTwoLayerClassifier();

		default:
			return null;
		}
	}
}

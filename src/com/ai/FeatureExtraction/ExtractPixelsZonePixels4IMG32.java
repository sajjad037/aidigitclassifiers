package com.ai.FeatureExtraction;

import com.ai.Model.DigitImage;

public class ExtractPixelsZonePixels4IMG32 implements IFeatureExtraction {

	@Override
	public DigitImage Extract(double[][] imgMatrix, int lable) {
		return new ExtractPixelsZone4IMG16().Extract(imgMatrix, lable, 4, 1);
	}
}

package com.ai.FeatureExtraction;

import com.ai.Model.DigitImage;

public class ExtractSUMOfPixels implements IFeatureExtraction {

	@Override
	public DigitImage Extract(double[][] imgMatrix, int lable) {
		 // 16 sum of column values, 16 sum of row values, 1
		// count of zero's, 1 count of one's, 1 actual
		// value.
		int rows = imgMatrix.length;
		int cols = imgMatrix[0].length;
		//int length = rows+cols+1+1+1;
		int length = rows+cols;
		double[] feature = new double[length];
		int featureIndex = 0;
		
		// Actual Value
		//feature[featureIndex] = lable;
		//++featureIndex;		
		
		// Sum of column values
		int sumOfCol = 0;
		for (int col = 0; col < cols; col++) {
			for (int row = 0; row < rows; row++) {
				sumOfCol += imgMatrix[row][col];
			}
			feature[featureIndex] = sumOfCol;
			++featureIndex;
			sumOfCol = 0;
		}

		// sum of row values
		int sumOfRow = 0;
		for (int i = 0; i < rows; i++) {

			for (int j = 0; j < cols; j++) {
				sumOfRow += imgMatrix[i][j];

			}
			feature[featureIndex] = sumOfRow;
			++featureIndex;
			sumOfRow = 0;
		}
		
//		 // Count # of 0's
//		 feature[featureIndex] = countValue(imgMatrix, 0);
//		 ++featureIndex;
//		
//		 // Count # of 1's
//		 feature[featureIndex] = countValue(imgMatrix, 1);
//		 ++featureIndex;
		
		//Normalize feature values into 0 and 1.
		feature = ExtractFectory.Normalize(feature);
		
		return new DigitImage(lable, feature);
	}
	
	private static int countValue(double[][] matrix, int value) {
		int count = 0;

		int rows = matrix.length;
		int cols = matrix[0].length;
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < cols; j++) {
				if (matrix[i][j] == value) {
					++count;
				}
			}
		}
		return count;
	}

}

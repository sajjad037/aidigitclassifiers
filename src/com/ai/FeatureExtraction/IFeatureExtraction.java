package com.ai.FeatureExtraction;

import com.ai.Model.DigitImage;

public interface IFeatureExtraction {
	DigitImage Extract(double[][] imgMatrix, int lable);
}

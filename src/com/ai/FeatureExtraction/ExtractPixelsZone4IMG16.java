package com.ai.FeatureExtraction;

import com.ai.Model.DigitImage;
import com.ai.config.ApplicationStatic;

public class ExtractPixelsZone4IMG16 implements IFeatureExtraction {

	@Override
	public DigitImage Extract(double[][] imgMatrix, int lable) {
		return Extract(imgMatrix, lable, 4, 1);
	}

	public DigitImage Extract(double[][] imgMatrix, int lable, int zoneSize, int valueCheck) {
		// Row and column must be Even
		int rows = imgMatrix.length;
		int cols = imgMatrix[0].length;
		// zoneSize must be even or complete divisible of Row and column
		// int zoneSize = 4;
		int featureLength = (rows / zoneSize) * (cols / zoneSize);
		double[] feature = new double[featureLength];
		
		int count = 0;

		int featureIndex = 0;

		for (int col = 0; col < cols; col++) {
			count = 0;
			for (int row = 0; row < rows; row++) {

				// Zone Vertical Loop
				for (int zo = 0; zo < zoneSize; zo++) {

					int inCol = col + zo;
					// Zone Horizontel Loop
					for (int zi = 0; zi < zoneSize; zi++) {
						int inRow = row + zi;
						if (imgMatrix[inCol][inRow] == valueCheck) {
							++count;
						}
					}
				}

				feature[featureIndex] = count;
				++featureIndex;
				count = 0;
				row = row + zoneSize - 1;
			}

			col = col + zoneSize - 1;
		}
		
		//Normalize feature values to 0 and 1.
		feature = ExtractFectory.Normalize(feature);
		
		return new DigitImage(lable, feature);
	}

}

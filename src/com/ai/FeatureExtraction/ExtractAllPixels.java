package com.ai.FeatureExtraction;

import com.ai.Model.DigitImage;

public class ExtractAllPixels implements IFeatureExtraction {

	@Override
	public DigitImage Extract(double[][] imgMatrix, int lable) {
		// convert all row to column
		int rows = imgMatrix.length;
		int cols = imgMatrix[0].length;
		int featureLength = rows * cols;
		double[] feature = new double[featureLength];

		int index= 0 ;
		// Sum of column values
		for (int col = 0; col < cols; col++) {
			for (int row = 0; row < rows; row++) {
				//index = col + row; 
				//feature[index] = imgMatrix[col][row];
				feature[index] = imgMatrix[col][row];
				++index;				
			}
		}

		//Here not need to normalize feature vector values already in 0 and 1 form.
		
		return new DigitImage(lable, feature);
	}

}


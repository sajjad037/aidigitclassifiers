package com.ai.FeatureExtraction;

import java.text.DecimalFormat;
import java.util.Arrays;

import com.ai.config.Enums.FeatureExteraction;

public class ExtractFectory {
	static DecimalFormat df = new DecimalFormat("#.##");
	public IFeatureExtraction getFeatureExtraction(FeatureExteraction featureExteraction) {
		switch (featureExteraction) {
		case AllPixels:
			return new ExtractAllPixels();

		case SUMOfPixels:
			return new ExtractSUMOfPixels();

		case PixelsZone4IMG16:
			return new ExtractPixelsZone4IMG16();

		case PixelsZone4IMG32:
			return new ExtractPixelsZonePixels4IMG32();

		default:
			return null;
		}
	}

	private static double Normalize(double value, double min, double max) {
		// Normalize the value (0-1).
		// X = (X - min) / (max - min)
		return  Double.valueOf(df.format((value - min) / (max - min)));
	}
	
	/**
	 * Normalize double into 1 and 0's
	 * @param inputValues
	 * @return
	 */
	public static double[] Normalize(double[] inputValues) {
		double min = minValue(inputValues);
		double max = maxValue(inputValues);		
		double[] values = new double[inputValues.length];
		for (int i = 0; i < inputValues.length; i++) {
			values[i] = Normalize(inputValues[i], min, max);
		}
		return values;
	}

	private static double maxValue(double array[]) {
		return Arrays.stream(array).max().getAsDouble();
	}
	
	private static double minValue(double array[]) {
		return Arrays.stream(array).min().getAsDouble();
	}
}

